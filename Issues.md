# Issues

Los issues son la puerta de entrada al proyecto, y por norma general son tareas que se tienen que atender: Pueden ser _bugs_ (errores) reportados que se tienen que resolver pero también tareas definidas pendientes de realizar, ya sean funcionalidades nuevas o deuda técnica.

Nos servirá como lugar donde recibir feedback del funcionamiento de la plataforma a la vez que será el lugar donde una contribuidora se informará de las tareas disponibles para empezar a trabajar. 

## Darles usos específicos

Uno de los primeros pasos que se debe tomar es definir que uso van a recibir los issues en el proyecto. Cada proyecto es distinto y el uso que se le den variará según las necesidades y características de este.

Por ejemplo, [fzf] los usa tanto para [reportar bugs] como para [peticiones de funcionalidades] o ideas en general. [Rails] por el contrario, usa los issues únicamente para reportar bugs. [Open Food Network] en cambio, los usa tanto para [errores] como para [deuda técnica] y el [desarrollo] general de la plataforma.

## Informar de errores

Un aspecto que la mayoría de proyectos comparten es tratar los issues como informes de errores. Al tratarse de algo que no requiere permisos especiales en el proyecto de Github permite que cualquiera reporte una incidencia.

Esto, además, da espacio para que la comunidad participe activamente y aporte información que ayude a entender las causas del error, más allá del equipo que lleve el día a día del proyecto.

Es importante que el issue sea muy concreto y no lleve a equívocos. Si queremos resolver un bug, tendremos que definir bien: 

* Como reproducirlo: pasos específicos que lo provocan
* Como tendría que funcionar correctamente

Y en general tantos detalles como sea posible del contexto en el que se ha ejecutado: versión del software, versión del navegador, sistema operativo, etc.

Si una contribuidora quiere resolver un issue, tiene que comentarlo en el propio issue, o asignárselo. De este modo otros pueden saber que este issue está ya está siendo tratado por alguien y no tendremos varias personas que tratan de resolver el mismo problema a la vez.

### Plantilla de issue

Github permite definir una plantilla para los issues. Una vez definida, Github muestra su contenido en el formulario de edición del issue. Esto facilita que los contribuidores aporten toda la información que es necesaria para que el issue sea útil. Lo cuál permite ganar eficiencia evitando la pérdida de tiempo y esfuerzo que conlleva tener que pedir más detalles al contribuidor o tener información incompleta.

Suele haber dos tipos generales de plantillas: las que contienen solo texto descriptivo pensado para ser leído una vez publicado el issue y los que contienen una checklist que tiene que seguir la persona antes de abrir el issue.

[github-issue-templates] es un buen ejemplo de plantillas ya definidas pensadas para ser reusadas, mientras que [awesome-github-templates] lista ejemplos de plantillas en proyectos ya existentes. Ambos son una buena manera de inspirarse para definir una que se adapte a nuestro proyecto.

[Homebrew-core] es un buen ejemplo de plantilla tipo checklist para un proyecto que trata los issues como informes de errores. Open Food Network, es otro ejemplo de proyecto con [múltiples plantillas] según la naturaleza del issue: error, funcionalidad, historia o deuda técnica.

Más detalles: [Issue and Pull Request templates]

## Establecer procesos

Una vez queda definido el uso que reciben los issues es necesario definir un proceso para tenerlos organizados y asegurar que se lleven a cabo. Sin sistematizar la manera como los vamos a procesar, por si solos, los issues simplemente se van a acumular.

Para ello en Github tenemos un herramientas principales, las [etiquetas], aunque los proyectos y milestones también nos pueden ayudar.

Las etiquetas nos permiten asignar categorías a los distintos tipos de issues y priorizarlos, lo que luego permite filtrarlos y hacerles un seguimiento.

Los [proyectos] permiten organizar los issues en listas. Esto ayuda mucho a organizar el trabajo cuando se utilizan metodologías de trabajo ágiles.

Por último, los [milestones] son otra manera de agrupar issues relacionados, ya sea porque van a formar parte de la misma release o porque parte de una misma funcionalidad.

En resumen, hay que encontrar el mecanismo que sea más conveniente, categorizar los issues y organizarlos para asegurar que se llevan a cabo, siempre teniendo en cuentas los objetivos y prioridades del proyecto.

Dar un vistazo: [How to be an open source gardener]<br>
Relacionado: [Metodologies de desenvolupament en obert]

[fzf]: https://github.com/junegunn/fzf
[reportar bugs]: https://github.com/junegunn/fzf/issues/1486
[peticiones de funcionalidades]: https://github.com/junegunn/fzf/issues/1482
[Rails]: https://github.com/rails/rails
[Open Food Network]: https://github.com/openfoodfoundation/openfoodnetwork
[errores]: https://github.com/openfoodfoundation/openfoodnetwork/issues?utf8=%E2%9C%93&q=is%3Aissue+is%3Aopen+bug
[deuda técnica]: https://github.com/openfoodfoundation/openfoodnetwork/labels/tech%20debt
[desarrollo]: https://github.com/openfoodfoundation/openfoodnetwork/issues/3465
[etiquetas]: https://help.github.com/en/articles/about-labels
[proyectos]: https://github.com/coopdevs/timeoverflow/projects/1
[milestones]: https://github.com/TryGhost/Ghost/milestone/47?closed=1
[How to be an open source gardener]: https://words.steveklabnik.com/how-to-be-an-open-source-gardener
[Metodologies de desenvolupament en obert]: https://speakerdeck.com/coopdevs/metodologies-de-desenvolupament-en-obert
[github-issue-templates]: https://github.com/stevemao/github-issue-templates
[awesome-github-templates]: https://github.com/devspace/awesome-github-templates
[Homebrew-core]: https://github.com/Homebrew/homebrew-core/blob/master/.github/ISSUE_TEMPLATE/Reproducible-Bug-Report.md
[múltiples plantillas]: https://github.com/openfoodfoundation/openfoodnetwork/issues/new/choose
[Issue and Pull Request templates]: https://github.blog/2016-02-17-issue-and-pull-request-templates/
