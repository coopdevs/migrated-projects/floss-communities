# Flujos de trabajo con Git

Debido a su naturaleza flexible no hay una sola manera estándar de trabajar con Git. Es por eso que hay varios flujos de trabajo ya establecidos. Estos no son nada más que recetas para llevar a cabo nuestro trabajo. Entre las que existen, a continuación explicamos brevemente *Feature Branch*, *Gitflow* y *Forking Workflow*. 

## Feature Branch Workflow

La idea del flujo de trabajo de rama por funcionalidad es que cualquier desarrollo sea una funcionalidad nuevo o un arreglo parta de la rama `master`. Esto permite trabajar en varias cosas en paralelo sin que varios desarrolladores se pisen. Este es por ejemplo el flujo de trabajo que se sigue en [certbot_nginx].

![Diagrama de coopdevs/certbot_nginx](img/certbot_nginx_workflow_diagram.png)

Para que luego el resto del equipo puede desarrollar encima de ese trabajo y los cambios acaben en producción, hace falta integrarlos mediante `git merge`.

Por tanto, en un repositorio que sigue este flujo de trabajo solo tendremos un rama `master` y tantas ramas como tareas distintas se estén trabajando. Dada su sencillez es por definición el flujo de trabajo que se utiliza cuando el trabajo implica a más de un desarrollador. Incluso en ocasiones es útil para organizarse el trabajo uno mismo.

## Gitflow Workflow

El Gitflow Workflow aporta más flexibilidad a costa de cierta complejidad. Este flujo de trabajo añade nuevas ramas con un propósito específico enfocadas a gestionar las releases de un proyecto.

### La rama develop

En primer lugar, la rama `develop` toma parte del rol que tiene `master` en Feature Branch. Las ramas que abramos parten y se integran con `develop`. Es por tanto, donde tendremos el código más reciente del proyecto. Esto, como veremos a continuación permite que `master` pasa a tener la verdad absoluta: código que funciona y que está en uso en producción.

Un ejemplo de este flujo de trabajo es [coopdevs.github.io].

![Diagrama de coopdevs/coopdevs.github.io](img/coopdevs.github.io_workflow_diagram.png)

### La rama release

Gitflow introduce la rama `release` como paso intermedio para poder sincronizar aquellos commits que están en `develop` pero que aun no han llegado a `master`.

Esta rama sirve para testear todos los cambios que se han aplicado des de la anterior versión publicada de forma conjunta. Usando esta rama podemos asegurar si todos los desarrollos llevados a cabo no introducen regresiones y funcionan correctamente combinados unos con otros. De no ser así, se hacen los arreglos necesarios hasta asegurarlo. En consecuencia, la rama `release` contendrá una serie commits que no existen ni en `master` ni en `develop`.

Para traer esos cambios cuando la release es correcta debemos integrar `release` con ambas ramas. Para ello tendremos que ejecutar dos `git merge` como se detalla a continuación.

```
$ git checkout develop
$ git pull develop # aseguramos que tenemos lo último
$ git merge release
$ git push develop # ya tenemos develop al dia
$ git checkout master
$ git pull master # también necesitamos lo último aquí
$ git merge release
$ git push master # hay que compartirlo con el resto del equipo
```

En este momento las tres ramas contienen el mismo estado y ya podemos traer la release a producción. Por otro lado, el resto del equipo ya puede seguir desarrollando la siguiente versión partiendo de `develop`.

[git-flow cheatsheet] contiene una buena explicación visual basada en una extensión de Git para Gitflow que puede servir de ayuda.

## Forking Workflow

Por último este flujo de trabajo se basa en la metodología que introduce GitHub y que ya se ha tratado en [Contribuir a un proyecto].

Independientemente de si optamos por Feature Branch o Gitflow, en el Forking Workflow no disponemos de un repositorio remoto central para el que todos los contribuidores tienen que tener permisos, sino que cada uno tiene su remoto. Esto es exactamente lo que sucede cuando contribuimos a un proyecto del que no formamos parte.

Esto aporta un control extra al equipo de mantenedores del proyecto permitiendo aceptar cambios de desarrolladores sin tener que dar permisos de escritura. Es por tanto un añadido a los flujos explicados anteriormente.

## Consideraciones

Hay que tener muy cuenta que el flujo de trabajo que escojamos se tiene que adaptar a la idiosincrasia y naturaleza del equipo y no al revés. Nos tiene que ayudar a desarrollar y no hacernos la vida más complicada.

Por ejemplo, si nuestro equipo es muy reducido lo más lógico será que optemos por Feature Branch. En cambio, si disponemos de un equipo de testing en el proyecto y varios desarrolladores trabajando en paralelo Gitflow puede tener más sentido.

En cualquiera de los casos procurar que **las ramas no contengan muchos cambios y tengan una vida corta** es una regla dorada para ahorrarnos conflictos al hacer merge. Eso conlleva que no solo nuestras ramas sino que todo nuestro trabajo esté compuesto de tareas pequeñas y de alcance asumible en todas las fases por las que pase: conceptualización, diseño, desarrollo, testing, etc. Es una idea fundamental para trabajar de forma ágil.

## Recursos

[Comparing Workflows](https://www.atlassian.com/git/tutorials/comparing-workflows): Comparativa y explicación de cada uno de los flujos de trabajo
[Git Branching - Branching Workflows](https://git-scm.com/book/en/v2/Git-Branching-Branching-Workflows): Consideraciones sobre el trabajo con ramas

[certbot_nginx]: https://github.com/coopdevs/certbot_nginx/network
[coopdevs.github.io]: https://github.com/coopdevs/coopdevs.github.io/network
[git-flow cheatsheet]: https://danielkummer.github.io/git-flow-cheatsheet/
[Contribuir a un proyecto]: Contribuir-a-un-proyecto.md
